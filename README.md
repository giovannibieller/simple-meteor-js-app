# SIMPLE METEOR JS APP #

Meteor.js app: React client + Mongo DB + Account Password

### DEV ###

* clone repository
* cd path/to/app
* ```npm install```
* ```meteor```
* app will run at ```http://localhost:3000```

### BUILD ###

NPM version: 4.6.1

* ```cd app/```
* ```npm run build```
* ```cd ../build/bundle```
* ```cd programs/server```
* ```npm install```
* ```export MONGO_URL='mongodb://127.0.0.1:27017/co2sample'```
* ```export ROOT_URL='http://127.0.0.1'```
* ```export MAIL_URL=''```
* ```export PORT=3000```
* ```cd ../../```
* ```node main.js```
* visit http://localhost:3000 to see app running

### NOTE ###

* la parte di export è spiegata dentro al README della /build sotto /build/bundle
- il setting della porta è opzionale. io l'ho fatto perché la 8080 di default non era disponibile sulla mia macchina
- ho segnato la versione di npm perché precedentemente avevo la 5.3 e non finiva l'npm install.
- è necessario avere installato ```meteor``` sulla propria macchina. Controllare che la versione di meteor npm -v sia corrispondente a npm -v (ho notato dava dei problemi con versioni differenti)