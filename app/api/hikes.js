import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Hikes = new Mongo.Collection('hikes');

if (Meteor.isServer) {
    Meteor.publish('hikes', function tasksPublication() {
        return Hikes.find({
            $or: [{ private: { $ne: true } }, { owner: this.userId }]
        });
    });
}

Meteor.methods({
    'hikes.insert'(name, altitude, done) {
        check(name, String);
        check(altitude, String);
        check(done, Boolean);

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        Hikes.insert({
            name: name,
            altitude: altitude,
            done: done,
            createdAt: new Date(),
            owner: Meteor.userId(),
            username: Meteor.user().username
        });
    },
    'hikes.remove'(hikeId) {
        check(hikeId, String);

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        Hikes.remove(hikeId);
    },
    'hikes.setHiked'(hikeId, setHiked) {
        check(hikeId, String);
        check(setHiked, Boolean);

        Hikes.update(hikeId, { $set: { done: setHiked } });
    },
    'hikes.setPrivate'(hikeId, setToPrivate) {
        check(hikeId, String);
        check(setToPrivate, Boolean);

        const hike = Hikes.findOne(hikeId);

        if (hike.owner !== Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        Hikes.update(hikeId, { $set: { private: setToPrivate } });
    }
});
