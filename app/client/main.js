import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import { FlowRouter } from 'meteor/kadira:flow-router';
import { mount, withOptions } from 'react-mounter';

import AppContainer from './components/app.js';
import Home from './components/home.js';
import List from './components/list.js';

import '../config/accounts-config.js';

const appMount = withOptions(
    {
        rootId: 'app'
    },
    mount
);

FlowRouter.route('/', {
    name: 'Home',
    action() {
        appMount(AppContainer, {
            main: <Home />
        });
    }
});

FlowRouter.route('/list', {
    name: 'Home',
    action() {
        appMount(AppContainer, {
            main: <List />
        });
    }
});
