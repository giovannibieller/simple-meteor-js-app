import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createContainer } from 'meteor/react-meteor-data';

import { Hikes } from '../../api/hikes.js';

import Hike from './hike.js';

class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            nameerror: false,
            altitude: '',
            altitudeerror: false,
            hideHiked: false,
            add: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.hideHiked = this.hideHiked.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleCloseAdd = this.handleCloseAdd.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        if (this.state.name !== '' && this.state.altitude !== '') {
            Meteor.call('hikes.insert', this.state.name, this.state.altitude, false);
            this.setState({
                add: false
            });
        } else {
            if (this.state.name === '') {
                this.setState({ nameerror: true });
            }
            if (this.state.altitude === '') {
                this.setState({ altitudeerror: true });
            }
        }
    }

    handleAdd() {
        this.setState({
            add: true,
            name: '',
            altitude: ''
        });
    }

    handleCloseAdd() {
        this.setState({
            add: false
        });
    }

    handleChange(e) {
        switch (e.target.id) {
            case 'name':
                this.setState({
                    name: e.target.value
                });
                break;
            case 'altitude':
                this.setState({
                    altitude: e.target.value
                });
                break;

            default:
                break;
        }
    }

    hideHiked(e) {
        e.preventDefault();
        this.setState({
            hideHiked: !this.state.hideHiked
        });
    }

    renderHikes() {
        if (this.props.hikes.length > 0) {
            let filteredHikes = this.props.hikes;
            if (this.state.hideHiked) {
                filteredHikes = filteredHikes.filter(hike => !hike.done);
            }

            return filteredHikes.map(hike => {
                const currentUserId = this.props.currentUser && this.props.currentUser._id;
                const showPrivateButton = hike.owner === currentUserId;
                return <Hike key={hike._id} hike={hike} showPrivateButton={showPrivateButton} />;
            });
        } else {
            let message = 'No hikes yet! Add one!';
            if (!this.props.currentUser) {
                message = 'No hikes yet! Register/Login and Add one!';
            }
            return (
                <h4>
                    {message}
                </h4>
            );
        }
    }

    render() {
        let nameclass = 'form-input',
            altitudeclass = 'form-input';
        if (this.state.nameerror) {
            nameclass += ' is-error';
        }
        if (this.state.altitudeerror) {
            altitudeclass += ' is-error';
        }
        return (
            <div className="list_container">
                {this.state.add &&
                    <div>
                        <h3>Add Hike</h3>
                        <form className="new-hike" onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <input
                                    className={nameclass}
                                    type="text"
                                    placeholder="hike name"
                                    id="name"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    className={altitudeclass}
                                    type="text"
                                    placeholder="hike altitude"
                                    id="altitude"
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="form-group">
                                <input type="submit" value="Add" className="btn btn-primary" />
                            </div>
                        </form>
                    </div>}

                {!this.state.add &&
                    <div className="list_container_int">
                        <h3 className="badge" data-badge={this.props.incompleteHikes}>
                            Hikes List&nbsp;
                        </h3>
                        {this.props.completeHikes > 0
                            ? <a className="complete_hider" href="#" onClick={this.hideHiked}>
                                  {!this.state.hideHiked ? 'Hide hiked' : 'Show hiked'}
                              </a>
                            : ''}

                        <div className="list_content">
                            {this.renderHikes()}
                        </div>
                    </div>}

                {this.props.currentUser &&
                    <div>
                        {!this.state.add
                            ? <button
                                  className="btn btn-primary btn-action btn-lg"
                                  onClick={this.handleAdd}
                              >
                                  <i className="icon icon-plus" />
                              </button>
                            : <button
                                  className="btn btn-primary btn-action btn-lg"
                                  onClick={this.handleCloseAdd}
                              >
                                  <i className="icon icon-cross" />
                              </button>}
                    </div>}
            </div>
        );
    }
}

List.propTypes = {
    hikes: PropTypes.array.isRequired,
    completeHikes: PropTypes.number.isRequired,
    incompleteHikes: PropTypes.number.isRequired,
    currentUser: PropTypes.object
};

export default createContainer(() => {
    Meteor.subscribe('hikes');

    return {
        hikes: Hikes.find({}, { sort: { altitude: -1 } }).fetch(),
        completeHikes: Hikes.find({ done: { $ne: false } }).count(),
        incompleteHikes: Hikes.find({ done: { $ne: true } }).count(),
        currentUser: Meteor.user()
    };
}, List);
