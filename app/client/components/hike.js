import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Hike extends Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.deleteHike = this.deleteHike.bind(this);
        this.toggleHike = this.toggleHike.bind(this);
        this.togglePrivate = this.togglePrivate.bind(this);
    }

    toggleHike() {
        Meteor.call('hikes.setHiked', this.props.hike._id, !this.props.hike.done);
    }

    togglePrivate() {
        Meteor.call('hikes.setPrivate', this.props.hike._id, !this.props.hike.private);
    }

    deleteHike() {
        Meteor.call('hikes.remove', this.props.hike._id);
    }

    render() {
        return (
            <div className="hike">
                {this.props.hike.done && <label className="chip">hiked</label>}
                {this.props.hike.private && <label className="chip">private</label>}
                <div className="text">
                    {this.props.hike.name + ' '}
                    <strong>
                        {this.props.hike.altitude}
                    </strong>
                </div>
                <button className="btn btn-primary" onClick={this.toggleHike}>
                    {!this.props.hike.done ? 'hiked' : 'not hiked'}
                </button>
                {this.props.showPrivateButton &&
                    <button className="btn btn-primary" onClick={this.togglePrivate}>
                        {!this.props.hike.private ? 'make it private' : 'make it public'}
                    </button>}

                {Meteor.userId() &&
                    <button className="btn" onClick={this.deleteHike}>
                        delete
                    </button>}
            </div>
        );
    }
}

Hike.propTypes = {
    hike: PropTypes.object.isRequired,
    showPrivateButton: PropTypes.bool.isRequired
};

export default Hike;
