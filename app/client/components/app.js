import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FlowRouter } from 'meteor/kadira:flow-router';

import { createContainer } from 'meteor/react-meteor-data';

import Account from './account.js';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.handleRoute = this.handleRoute.bind(this);
    }

    handleRoute(e) {
        e.preventDefault();

        switch (e.target.id) {
            case 'home':
                FlowRouter.go('/');
                break;
            case 'list':
                FlowRouter.go('/list');
                break;
            case 'add':
                FlowRouter.go('/add');
                break;
            default:
                FlowRouter.go('/');
                break;
        }
    }

    render() {
        return (
            <div>
                <header>
                    <nav>
                        <a href="#" id="home" onClick={this.handleRoute}>
                            HOME
                        </a>
                        <a href="#" id="list" onClick={this.handleRoute}>
                            LIST
                        </a>
                    </nav>

                    <Account />
                </header>

                <div className="main_container">
                    {this.props.main}
                </div>
            </div>
        );
    }
}

export default (AppContainer = createContainer(props => {
    // props here will have `main`, passed from the router
    // anything we return from this function will be *added* to it
    return {};
}, App));
