import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createContainer } from 'meteor/react-meteor-data';

import { Accounts } from 'meteor/accounts-base';

class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            userfirstname: '',
            useremail: '',
            userpassword: '',
            useremailLogin: '',
            userpasswordLogin: ''
        };

        this.handleModal = this.handleModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleSubmitRegister = this.handleSubmitRegister.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleModal() {
        this.setState({
            open: !this.state.open
        });
    }

    handleChange(e) {
        switch (e.target.id) {
            case 'name':
                this.setState({
                    userfirstname: e.target.value
                });
                break;
            case 'email':
                this.setState({
                    useremail: e.target.value
                });
                break;
            case 'password':
                this.setState({
                    userpassword: e.target.value
                });
                break;

            default:
                break;
        }
    }

    handleChangeLogin(e) {
        switch (e.target.id) {
            case 'email':
                this.setState({
                    useremailLogin: e.target.value
                });
                break;
            case 'password':
                this.setState({
                    userpasswordLogin: e.target.value
                });
                break;

            default:
                break;
        }
    }

    handleSubmitRegister(e) {
        e.preventDefault();

        let user = {
            username: this.state.useremail,
            email: this.state.useremail,
            password: this.state.userpassword,
            profile: {
                name: this.state.userfirstname
            }
        };

        let cb = () => {
            console.log('registered');
            let cbLogin = () => {
                console.log('logged in');
                this.handleModal();
            };
            Meteor.loginWithPassword(this.state.useremail, this.state.userpassword, cbLogin);
        };
        Accounts.createUser(user, cb);
    }

    handleLogin(e) {
        e.preventDefault();

        let user = {
            username: this.state.useremailLogin,
            password: this.state.userpasswordLogin
        };

        let cb = () => {
            console.log('logged in');
            this.handleModal();
        };

        Meteor.loginWithPassword(this.state.useremailLogin, this.state.userpasswordLogin, cb);
    }

    handleLogout() {
        Meteor.logout();
    }

    render() {
        let modalClasses = 'modal';
        if (this.state.open) {
            modalClasses = 'modal active';
        }

        let loggedin = this.props.currentUser,
            userName = '';

        if (loggedin) {
            userName = this.props.currentUser.profile.name;
        }

        return (
            <div className="account">
                {loggedin ? 'Hi ' + userName + ' | ' : ''}
                <a
                    href="#"
                    className="login"
                    onClick={!loggedin ? this.handleModal : this.handleLogout}
                >
                    {!loggedin ? 'LOGIN/REGISTER' : 'LOGOUT'}
                </a>
                <div className={modalClasses}>
                    <div className="modal-overlay" />
                    <div className="modal-container">
                        <div className="modal-header">
                            <button
                                className="btn btn-clear float-right"
                                onClick={this.handleModal}
                            />
                            <div className="modal-title">Modal title</div>
                        </div>
                        <div className="modal-body">
                            <div className="columns">
                                <div className="column col-6">
                                    <div className="content">
                                        <p>
                                            <strong>REGISTER</strong>
                                        </p>
                                        <form
                                            className="account-form"
                                            onSubmit={this.handleSubmitRegister}
                                        >
                                            <div className="form-group">
                                                <input
                                                    className="form-input"
                                                    type="text"
                                                    placeholder="name"
                                                    id="name"
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    className="form-input"
                                                    type="email"
                                                    placeholder="email"
                                                    id="email"
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    className="form-input"
                                                    type="password"
                                                    placeholder="password"
                                                    id="password"
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                            <button type="submit" className="btn btn-primary">
                                                Register
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                <div className="column col-6">
                                    <div className="content">
                                        <p>
                                            <strong>LOGIN</strong>
                                        </p>
                                        <form className="account-form" onSubmit={this.handleLogin}>
                                            <div className="form-group">
                                                <input
                                                    className="form-input"
                                                    type="email"
                                                    placeholder="email"
                                                    id="email"
                                                    onChange={this.handleChangeLogin}
                                                />
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    className="form-input"
                                                    type="password"
                                                    placeholder="password"
                                                    id="password"
                                                    onChange={this.handleChangeLogin}
                                                />
                                            </div>
                                            <button type="submit" className="btn btn-primary">
                                                Login
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Account.propTypes = {
    currentUser: PropTypes.object
};

export default createContainer(() => {
    return {
        currentUser: Meteor.user()
    };
}, Account);
